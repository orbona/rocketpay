defmodule Rocketpay.NumbersTest do
  use ExUnit.Case
  alias Rocketpay.Numbers

  describe "sum_from_file/1" do
    test "when thes is a file with given a name, return sum of numbers" do
      response = Numbers.sum_from_file("numbers")
      expected_response = {:ok, %{result: 55}}
      assert response == expected_response
    end

    test "when thes is no file with given a name, return a error" do
      response = Numbers.sum_from_file("banana")
      expected_response = {:error, %{message: "invalid file!"}}
      assert response == expected_response
    end
  end
end
